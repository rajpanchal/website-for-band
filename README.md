# Fiction Band
This is simple website for fictional Band which is made using `flex` property of CSS3.

## Build With
1. HTML
2. CSS3

## License
MIT © [Raj Panchal](https://gitlab.com/rajpanchal)